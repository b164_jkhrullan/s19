console.log("Hello World!");

const cube = 2 ** 3 ;

console.log(`The cube of 2 is ${cube}`);

const address = ["258 Washington Ave NW, California", "90011"];

console.log(address[0]);
console.log(address[1]);

console.log(`I live at ${address[0]} ${address[1]}`);

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in",
}

const{name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`)

let numbers = [10, 29];


numbers.forEach(num => console.log(numbers));

const add = (x, y) => x + y;

let total = add(10, 29);
console.log(total);

class dog {
	constructor(name, age, breed) {
		this.name = "Sheldon";
		this.age = 1;
		this.breed = "Mutt with Golden Retriever, Husky and Australian Shepherd";
	}
}

const myDog = new dog();

console.log(myDog);




